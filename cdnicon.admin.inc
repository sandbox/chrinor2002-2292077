<?php

/**
 * Callback for main settings for for CDNIcon configuration.
 * @param type $form
 * @param type $form_state
 * @return type
 */
function cdnicon_settings_form($form, $form_state){
  $cdnicon_set = variable_get('cdnicon_seturl', '');
  $form['cdnicon_seturl'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon Pack url'),
    '#default_value' => $cdnicon_set,
    '#field_prefix' => _cdnicon_get_configurl(),
    '#element_validate' => array('cdnicon_settings_seturl_validate'),
  );
  if($cdnicon_set){
    $form['glyph-list'] = array(
      '#theme' => 'cdnicon_glyphs_list',
      '#configurl' => cdnicon_get_configurl($cdnicon_set),
    );
  }

  $cdnicon_includeall = variable_get('cdnicon_includeall', FALSE);
  $form['cdnicon_includeall'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include as Theme Resource'),
    '#description' => t('If checked, this will enable including of the icon CSS on every page load.'),
    '#default_value' => $cdnicon_includeall,
  );

  return system_settings_form($form);
}

/**
 * Ensures that settings are reset when things are changed.
 */
function cdnicon_settings_form_submit($form, &$form_state){
  // TOOD: check new to old to ensure no broken icons.
  cache_clear_all('cdnicon:', 'cache');
}
